# OpenML dataset: Daily-Wheat-Price

https://www.openml.org/d/43601

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Last time I built an LSTM price prediction for Corn, but the result is not satisfactory. I would like to try other algorithm and data. So I decided to use Wheat price for the exercise. And this time the data is more in length of time (9 years).
Content
The daily wheat dataset is from 2009-10-14 to 2018-03-12. It is downloaded from investing.com or Quantapi also have a API for it. 
Acknowledgements
https://www.investing.com/commodities/us-wheat

https://quantapi.co/
Inspiration
To the extent that you can find ways where you're making predictions, there's no substitute for testing yourself on real-world situations that you don't know the answer to in advance. 
Nate Silver

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43601) of an [OpenML dataset](https://www.openml.org/d/43601). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43601/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43601/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43601/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

